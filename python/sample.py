from Crypto.Cipher import AES
from hashlib import sha256
import sys
import os
import random
import struct

block_size = 4096
def encrypt_file(passwd, infile, outfile=None):
	if infile == None or passwd == None:
		return
	if not outfile:
			outfile = infile+'_enc'
        key = sha256(passwd).digest()
        random.seed()
        iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
        cipher = AES.new(key, AES.MODE_CFB, iv)
        with open(infile,'r') as ifh, open(outfile, 'w') as ofh:
            '''
            filesize = os.path.getsize(infile)
            buff = struct.pack('<Q', filesize)
            buff+=''.join(chr(random.randint(0, 0xFF)) for i in
                    range(block_size-len(buff)))
            '''
            buff = iv
            buff+=''.join(chr(random.randint(0, 0xFF)) for i in
                    range(block_size-len(buff)))
            ofh.write(cipher.encrypt(buff))
	    while True:
		buff = ifh.read(block_size)
		if len(buff) == 0:
		    break
		elif len(buff) < block_size:
                    pass
                    #buff+=''.join(chr(random.randint(0, 0xFF)) for i in
                    #        range(block_size-len(buff)))
		enc_buff = cipher.encrypt(buff)
		ofh.write(enc_buff)

def decrypt_file(passwd, infile, outfile=None):
	if infile == None or passwd == None:
		return
	if not outfile:
			outfile = infile+'_dec'
        key = sha256(passwd).digest()
        iv = 16 * '\x00'
	cipher = AES.new(key, AES.MODE_CFB, iv)
	with open(infile,'r') as ifh, open(outfile, 'w') as ofh:
            buff = cipher.decrypt(ifh.read(block_size))
            iv = buff[:16]
            '''
            filesize = struct.unpack('<Q', buff[:struct.calcsize('Q')])[0]
            lb_size = filesize%block_size
            last_buff = None
            buff = None
            '''
	    while True:
                #last_buff = buff
		buff = ifh.read(block_size)
		if len(buff) == 0:
                    #if last_buff != None:
                    #    last_buff = last_buff[0:lb_size]
                    #    ofh.write(last_buff)
		    break
                #if last_buff != None:
                #    ofh.write(last_buff)
		buff = cipher.decrypt(buff)
                ofh.write(buff)
	
if __name__ == "__main__":
	if sys.argv[1] == 'encrypt':
		encrypt_file(sys.argv[2], sys.argv[3])
	elif sys.argv[1] == 'decrypt':
		decrypt_file(sys.argv[2], sys.argv[3])
	else:
		print "Usage: python aes <passwd> <file>"
