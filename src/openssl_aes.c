#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <openssl/aes.h>

int encrypt(char *iv, const char *key, char *plaintext,
				char* ciphertext, int len)
{
	AES_KEY enc_key;
	AES_set_encrypt_key(key, 128, &enc_key);
	int num = 0;

	AES_cfb128_encrypt(plaintext, ciphertext, len, &enc_key, iv,
					&num, AES_ENCRYPT);
	
	return 0;
}

int decrypt(char *iv, const char *key, char *ciphertext,
				char* plaintext, int len)
{
	AES_KEY dec_key;
	AES_set_encrypt_key(key, 128, &dec_key);
	int num = 0;

	AES_cfb128_encrypt(ciphertext, plaintext, len, &dec_key, iv,
					&num, AES_DECRYPT);

	return 0;
}

int main(int argc, char **argv)
{
    FILE *fp1, *fp2;
	static const unsigned char key[] = {
		0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
		0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	};
	static const unsigned char iv[] = {
		0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
		0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	};
	char *iv_input = NULL;
	char *iv_output = NULL;
	char *plaintext = NULL;
	char *ciphertext = NULL;
    int ret;
	off_t i = 0;
	struct stat buffer;
	off_t size;
	off_t total_blocks;

    fp1 = fopen("/tmp/test.txt", "r+");
    fp2 = fopen("/tmp/test.enc", "w+");
	stat("/tmp/test.txt", &buffer);
	size = buffer.st_size;
	total_blocks = size/4096;
	if (size%4096 != 0)
		total_blocks++;

#pragma omp parallel private(plaintext, ciphertext, iv_input, iv_output, ret)
{
	ret = 0;
    plaintext = malloc(4096);
    ciphertext = malloc(4096);
	iv_input = (char *) malloc(16);
	iv_output = (char *) malloc(16);
	#pragma omp for
	for (i = 0; i < total_blocks; i++) {
		memset(plaintext, '\0', 4096);
		#pragma omp critical
		{
			fseek(fp1, 4096*i, SEEK_SET);
			ret = fread(plaintext, 1, 4096, fp1);
		}
		memcpy(iv_input, iv, 16);
        encrypt(iv_input, key, plaintext, ciphertext, ret);
		#pragma omp critical
		{
			fseek(fp2, 4096*i, SEEK_SET);
        	fwrite(ciphertext, 1, ret, fp2);
		}
	}
	free(ciphertext);
	free(plaintext);
	free(iv_input);
	free(iv_output);
}

	fclose(fp1);
	fclose(fp2);

	fp1 = fopen("/tmp/test.enc", "r+");
    fp2 = fopen("/tmp/test.dec", "w+");
	stat("/tmp/test.enc", &buffer);
	size = buffer.st_size;
	total_blocks = size/4096;
	if (size%4096 != 0)
		total_blocks++;
	
#pragma omp parallel private(plaintext, ciphertext, iv_input, iv_output, ret)
{
	ret = 0;
    plaintext = malloc(4096);
    ciphertext = malloc(4096);
	iv_input = (char *) malloc(16);
	iv_output = (char *) malloc(16);
	#pragma omp for
	for (i = 0; i < total_blocks; i++) {
		memset(ciphertext, '\0', 4096);
		#pragma omp critical
		{
			fseek(fp1, 4096*i, SEEK_SET);
			ret = fread(ciphertext, 1, 4096, fp1);
		}
		memcpy(iv_input, iv, 16);
        decrypt(iv_input, key, ciphertext, plaintext, ret);
		#pragma omp critical
		{
			fseek(fp2, 4096*i, SEEK_SET);
        	fwrite(plaintext, 1, ret, fp2);
		}
	}
	free(ciphertext);
	free(plaintext);
	free(iv_input);
	free(iv_output);
}
	fclose(fp1);
	fclose(fp2);
    return 0;
}
