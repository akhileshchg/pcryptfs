#include "pcryptfs.h"

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

static int pcryptfs_error(char *str)
{
    int ret = -errno;
    return ret;
}

static void pcryptfs_fullpath(char fpath[PATH_MAX], const char *path)
{
    strcpy(fpath, PCRYPTFS_DATA->rootdir);
    strncat(fpath, path, PATH_MAX);
}

int pcryptfs_getattr(const char *path, struct stat *statbuf)
{
    int retstat = 0;
    char fpath[PATH_MAX];

    pcryptfs_fullpath(fpath, path);
    retstat = lstat(fpath, statbuf);
    if (retstat != 0)
		retstat = pcryptfs_error("pcryptfs_getattr lstat");
    return retstat;
}

int pcryptfs_readlink(const char *path, char *link, size_t size)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    retstat = readlink(fpath, link, size - 1);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_readlink readlink");
    else  {
		link[retstat] = '\0';
		retstat = 0;
    }
    return retstat;
}

int pcryptfs_mknod(const char *path, mode_t mode, dev_t dev)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
   	retstat = mknod(fpath, mode, dev); 
	if (retstat < 0)
		retstat = -errno;
    return retstat;
}

int pcryptfs_mkdir(const char *path, mode_t mode)
{
    int retstat = 0;
    char fpath[PATH_MAX];

    pcryptfs_fullpath(fpath, path);
    retstat = mkdir(fpath, mode);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_mkdir mkdir");
    return retstat;
}

int pcryptfs_unlink(const char *path)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    retstat = unlink(fpath);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_unlink unlink");
    return retstat;
}

int pcryptfs_rmdir(const char *path)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    retstat = rmdir(fpath);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_rmdir rmdir");
    return retstat;
}

int pcryptfs_symlink(const char *path, const char *link)
{
    int retstat = 0;
    char flink[PATH_MAX];
    
    pcryptfs_fullpath(flink, link);
    retstat = symlink(path, flink);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_symlink symlink");
    return retstat;
}

int pcryptfs_rename(const char *path, const char *newpath)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    char fnewpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    pcryptfs_fullpath(fnewpath, newpath);
    retstat = rename(fpath, fnewpath);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_rename rename");
    return retstat;
}

int pcryptfs_link(const char *path, const char *newpath)
{
    int retstat = 0;
    char fpath[PATH_MAX], fnewpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    pcryptfs_fullpath(fnewpath, newpath);
    retstat = link(fpath, fnewpath);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_link link");
    return retstat;
}

int pcryptfs_chmod(const char *path, mode_t mode)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    retstat = chmod(fpath, mode);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_chmod chmod");
    return retstat;
}

int pcryptfs_chown(const char *path, uid_t uid, gid_t gid)
  
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    retstat = chown(fpath, uid, gid);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_chown chown");
    return retstat;
}

int pcryptfs_truncate(const char *path, off_t newsize)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    retstat = truncate(fpath, newsize);
    if (retstat < 0)
		pcryptfs_error("pcryptfs_truncate truncate");
    return retstat;
}

int pcryptfs_utime(const char *path, struct utimbuf *ubuf)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    retstat = utime(fpath, ubuf);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_utime utime");
    return retstat;
}

int pcryptfs_open(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    int fd;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    fd = open(fpath, fi->flags);
    if (fd < 0)
		retstat = pcryptfs_error("pcryptfs_open open");
    
    fi->fh = fd;
    return retstat;
}

int pcryptfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int retstat = 0;
   /*
	* * Use getattr and find if the fi->fh is a directory.
	* * If not proceed with the following decryption.
	*
	* * Calcuate the offset of the starting block from which
	* decryption should start using 'offset' argument.
	*
	* * Based on the above offset and size, decide total number of
	* blocks to be read using PAGE_SIZE
	* 
	* * Read every block, decrypt and place the decrypted buffer in
	* buf
	*
	* * CATCH: Care should be taken on final block of decyrption in
	* loop if the encyrption algorithm used is variable length buffer
	* encyrption algo.
	*
	* * Following this, make stuff parallel
	*/
    retstat = pread(fi->fh, buf, size, offset);
	printf("PID: %d\n", getpid());
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_read read");
    return retstat;
}

int pcryptfs_write(const char *path, const char *buf, size_t size, off_t offset,
	     struct fuse_file_info *fi)
{
    int retstat = 0;
    
    retstat = pwrite(fi->fh, buf, size, offset);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_write pwrite");
    return retstat;
}

int pcryptfs_statfs(const char *path, struct statvfs *statv)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    retstat = statvfs(fpath, statv);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_statfs statvfs");
    return retstat;
}

int pcryptfs_flush(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    return retstat;
}

int pcryptfs_release(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    retstat = close(fi->fh);
    return retstat;
}

int pcryptfs_fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
    int retstat = 0;
    
#ifdef HAVE_FDATASYNC
    if (datasync)
	retstat = fdatasync(fi->fh);
    else
#endif	
	retstat = fsync(fi->fh);
    
    if (retstat < 0)
	retstat = pcryptfs_error("pcryptfs_fsync fsync");
    
    return retstat;
}

int pcryptfs_opendir(const char *path, struct fuse_file_info *fi)
{
    DIR *dp;
    int retstat = 0;
    char fpath[PATH_MAX];
    
    pcryptfs_fullpath(fpath, path);
    dp = opendir(fpath);
    if (dp == NULL)
		retstat = pcryptfs_error("pcryptfs_opendir opendir");
    fi->fh = (intptr_t) dp;
    return retstat;
}

int pcryptfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset,
	       struct fuse_file_info *fi)
{
    int retstat = 0;
    DIR *dp;
    struct dirent *de;
    
    dp = (DIR *) (uintptr_t) fi->fh;
    de = readdir(dp);
    if (de == 0) {
		retstat = pcryptfs_error("pcryptfs_readdir readdir");
		return retstat;
    }

    do {
		if (filler(buf, de->d_name, NULL, 0) != 0) {
	    	return -ENOMEM;
		}
    } while ((de = readdir(dp)) != NULL);
    
    return retstat;
}

int pcryptfs_releasedir(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    closedir((DIR *) (uintptr_t) fi->fh);
    return retstat;
}

int pcryptfs_fsyncdir(const char *path, int datasync, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    return retstat;
}

void *pcryptfs_init(struct fuse_conn_info *conn)
{
    return PCRYPTFS_DATA;
}

void pcryptfs_destroy(void *userdata)
{
}

int pcryptfs_access(const char *path, int mask)
{
    int retstat = 0;
    char fpath[PATH_MAX];
   
    pcryptfs_fullpath(fpath, path);
    retstat = access(fpath, mask);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_access access");
    return retstat;
}

int pcryptfs_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    int fd;
    
    pcryptfs_fullpath(fpath, path);
    fd = creat(fpath, mode);
    if (fd < 0)
		retstat = pcryptfs_error("pcryptfs_create creat");
    fi->fh = fd;
    return retstat;
}

int pcryptfs_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    retstat = ftruncate(fi->fh, offset);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_ftruncate ftruncate");
    return retstat;
}

int pcryptfs_fgetattr(const char *path, struct stat *statbuf, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    if (!strcmp(path, "/"))
		return pcryptfs_getattr(path, statbuf);
    
    retstat = fstat(fi->fh, statbuf);
    if (retstat < 0)
		retstat = pcryptfs_error("pcryptfs_fgetattr fstat");
    
    return retstat;
}

struct fuse_operations pcryptfs_ops = {
  .getattr = pcryptfs_getattr,
  .readlink = pcryptfs_readlink,
  // no .getdir -- that's deprecated
  .getdir = NULL,
  .mknod = pcryptfs_mknod,
  .mkdir = pcryptfs_mkdir,
  .unlink = pcryptfs_unlink,
  .rmdir = pcryptfs_rmdir,
  .symlink = pcryptfs_symlink,
  .rename = pcryptfs_rename,
  .link = pcryptfs_link,
  .chmod = pcryptfs_chmod,
  .chown = pcryptfs_chown,
  .truncate = pcryptfs_truncate,
  .utime = pcryptfs_utime,
  .open = pcryptfs_open,
  .read = pcryptfs_read,
  .write = pcryptfs_write,
  .statfs = pcryptfs_statfs,
  .flush = pcryptfs_flush,
  .release = pcryptfs_release,
  .fsync = pcryptfs_fsync,
  .opendir = pcryptfs_opendir,
  .readdir = pcryptfs_readdir,
  .releasedir = pcryptfs_releasedir,
  .fsyncdir = pcryptfs_fsyncdir,
  .init = pcryptfs_init,
  .destroy = pcryptfs_destroy,
  .access = pcryptfs_access,
  .create = pcryptfs_create,
  .ftruncate = pcryptfs_ftruncate,
  .fgetattr = pcryptfs_fgetattr
};

void pcryptfs_usage()
{
    fprintf(stderr, "usage: mount.pcryptfs <passwd> <root_dir> <mount_point>\n");
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
    int fuse_stat;
    struct pcryptfs_state *pcryptfs_data;

    if (argc != 4)
		pcryptfs_usage();

    pcryptfs_data = malloc(sizeof(struct pcryptfs_state));
    if (pcryptfs_data == NULL) {
		perror("main calloc");
		abort();
    }
	pcryptfs_data->key = argv[1];
	pcryptfs_data->rootdir = realpath(argv[2], NULL);
	argv[1] = argv[3];
	argv[2] = NULL;
	argv[3] = NULL;
	argc-=2;
    fprintf(stderr, "about to call fuse_main\n");
    fuse_stat = fuse_main(argc, argv, &pcryptfs_ops, pcryptfs_data);
    fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
    
    return fuse_stat;
}
