/*
  Copyright (C) 2012 Joseph J. Pfeiffer, Jr., Ph.D. <pfeiffer@cs.nmsu.edu>

  This program can be distributed under the terms of the GNU GPLv3.
  See the file COPYING.

  There are a couple of symbols that need to be #defined before
  #including all the headers.
*/

#ifndef _PARAMS_H_
#define _PARAMS_H_

#define FUSE_USE_VERSION 26
#define _XOPEN_SOURCE 500

#include <limits.h>
#include <stdio.h>
struct pcryptfs_state {
    const char *rootdir;
	const char *key;
};
#define PCRYPTFS_DATA ((struct pcryptfs_state *) fuse_get_context()->private_data)

#endif
